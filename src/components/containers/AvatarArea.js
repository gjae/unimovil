import React from 'react';
import { connect } from 'react-redux';
import Avatar from '../presentation/ui/AvatarArea';

function mapStateToProps(state, ownProps) {
	return {
		show_name: ownProps.name+' '+ownProps.lastname,
		avatar: ownProps.picture
	}
}

const AvatarArea = connect(mapStateToProps, null)(Avatar);

export default AvatarArea;