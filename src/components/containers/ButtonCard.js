import React from 'react';
import Button from '../presentation/ui/ButtonCard';
import { connect } from 'react-redux';

function mapStateToProps(state, ownProps){
	return {
		button_text: ownProps.button_text,
		icon_name: ownProps.icon_name,
		icon_color: ownProps.icon_name,
		styles: ownProps.styles
	}
}

const ButtonCard = connect(mapStateToProps, null)(Button);

export default ButtonCard;