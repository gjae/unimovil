import React from 'react';
import LoginForm from '../presentation/ui/LoginForm';
import { connect } from 'react-redux';
import { AsyncStorage, Alert } from 'react-native';

import { Users } from '../../config/Storage';
import {sendCredentials} from '../../redux/actions';
import { SEND_CREDENTIALS } from '../../redux/actions';

function mapDispatchToProps(dispatch, ownProps){
	var loginText = '';
	var password = '';
	return {
		onChangeLoginText: (text) => {
			loginText = text;
		},
		onChangePassword: (text) => {
			password = text;
		},
		onPress: ()=>{
		//	Alert.alert("Datos ingresados", 'Login: '+loginText+' password: '+password);
			credentials = {loginText, password};
			dispatch( sendCredentials( credentials, ownProps.navigation ) );
		}
	}
}

const Form = connect(null, mapDispatchToProps)(LoginForm);

export default Form;