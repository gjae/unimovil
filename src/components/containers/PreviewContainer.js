import React from 'react';
import  Preview from '../presentation/ui/PreviewNotification';
import { connect } from 'react-redux';


const mapStateToProps = (state) =>{
	return state;
}


const Notification = connect(mapStateToProps, null)(Preview);

export default Notification;