import React from 'react';
import {
	View,
	Text,
	TouchableOpacity,
	Alert
} from 'react-native';
import {
	Grid,
	Col,
	Row
} from 'react-native-easy-grid';


import {
	LoginStyles,
	HomeStyles
} from '../styles';

import { Card } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';

import ButtonCard from '../../containers/ButtonCard';
import AvatarArea from '../../containers/AvatarArea';

class HomeScreen extends React.Component{

	constructor(props){
		super(props);
	}

	render(){
		return(
			<Grid style={LoginStyles.botStyles}>
				<Row size={20} style={LoginStyles.topStyles} >
					<AvatarArea name={'Giovanny'} lastname={'Avila'} picture={require('../../../assets/img/avatar1.png')} />
				</Row>
				<Row size={17} style={ { marginTop: 8 } }>

					<ButtonCard 
						icon_name={"ios-person"} 
						icon_color={LoginStyles.topStyles.backgroundColor} 
						styles={HomeStyles.cardsStyles} 
						button_text={"Mis datos"} 
					/>

					<ButtonCard 
						icon_name={"ios-calendar"} 
						icon_color={LoginStyles.topStyles.backgroundColor} 
						styles={HomeStyles.cardsStyles} 
						button_text={"Horario"} 
					/>

					<ButtonCard 
						icon_name={"ios-book"} 
						icon_color={LoginStyles.topStyles.backgroundColor} 
						styles={HomeStyles.cardsStyles} 
						button_text={"Materias"} 
					/>
				</Row>
				<Row size={40} />
			</Grid>
		)
	}
}
export default HomeScreen;