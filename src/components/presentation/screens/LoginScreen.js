import React from 'react';
import {
	Row,
	Col,
	Grid
} from 'react-native-easy-grid';

import {
	Text,
	StatusBar,
	View,
	Image,
	Alert,
	AsyncStorage
} from 'react-native';

import {
	LoginStyles
} from '../styles';

import {
	Card
} from 'react-native-elements';

import Form from '../../containers/LoginForm';

import { PermissionsAndroid } from 'react-native';


export default class LoginScreen extends React.Component{

	constructor(props){
		super(props);
	}
	render(){	
		return (
			<Grid>
				<StatusBar
					backgroundColor={LoginStyles.statusBarStyles.backgroundColor}
				/>
				<Row style={LoginStyles.topStyles} size={25}>
					<Col style={{ alignItems: 'center', alignSelf: 'center' }}>
						<Image source={require('../../../assets/img/logotransparente_240px.png')}  />
					</Col>	
				</Row>
				<Row style={LoginStyles.botStyles} size={40}>
					<Col >
						<Card style={ LoginScreen.cardStyles }>
							<Form navigation={this.props.navigation} />
						</Card>
					</Col>
				</Row>
			</Grid>
		);
	}
}