import React from 'react';
import { View, Text, Alert, ActivityIndicator, ScrollView, FlatList, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import { Col, Grid, Row } from 'react-native-easy-grid';
import Notification from '../ui/Notification'
import { connect } from 'react-redux';
import { GET_NOTIFICATIONS, SET_NOTIFICATIONS_FILTER, notifications, viewNotifications } from '../../../redux/actions';
import rootReducer from '../../../redux/reducers';
import { Divider, CheckBox } from 'react-native-elements';

import Collapsible from 'react-native-collapsible';
import Icon from 'react-native-vector-icons/Ionicons';

import {
	LoginStyles
} from '../styles';


var { height, width } = Dimensions.get('window');

const styles = StyleSheet.create({ 

	screenTitle: {
		height: "9.5%", 
		paddingLeft: 4, 
		paddingTop: 14, 
		backgroundColor: LoginStyles.topStyles.backgroundColor 
	},
	collapseMenu: {
		backgroundColor: LoginStyles.topStyles.backgroundColor,
		height: ( height * 10 ) / 100
	},

	containerCheckboxStyle: {
		backgroundColor: 'transparent',
		borderWidth: 0
	},

	textCheckboxStyle:{
		color: "#FFFFFF"
	}
});


class NotificationsScreen extends React.Component{

	constructor(props){
		super(props);
		this._renderNotifications.bind(this);
		this.state = {
			isCollapse: true,
			viewAll: true
		}
	}

	componentWillMount(){
		this.props.dispatch();
	}


	_renderItem = ({item}) =>{
		let notification = item;

		if( this.state.viewAll )
			return (
				<Row key={(notification.id+'-ROW')} style={{ backgroundColor: '#ffffff', marginBottom: 1.3, elevation: 0.2 }} >
					<Notification 
						key={notification.id} 
						title={notification.title} 
						short_text={ notification.short_text } 
						favorite={notification.favorite}
						unFavoriteColor={"#000000"}
						favoriteColor={LoginStyles.topStyles.backgroundColor}
					/>
				</Row>
			);
		else if( notification.favorite ) 
			return (
				<Row key={(notification.id+'-ROW')} style={{ backgroundColor: '#ffffff', marginBottom: 1.3, elevation: 0.2 }} >
					<Notification 
						key={notification.id} 
						title={notification.title} 
						short_text={ notification.short_text } 
						favorite={notification.favorite}
						unFavoriteColor={"#000000"}
						favoriteColor={LoginStyles.topStyles.backgroundColor}
					/>
				</Row>
			);
	}

	_onCheckboxesPresed = (viewAll = true) =>{
		var { notifications } = this.props;

		this.setState({
			viewAll
		});
	}

	_renderNotifications(){
		if( this.props.isFetch ){
			return <ActivityIndicator />
		}else {
			let { notifications } = this.props;

			return <FlatList extraData={this.state.viewAll} data={ notifications } renderItem={this._renderItem}  />
		}
	}

	render(){
		return(
			<Grid>
				<Row style={ styles.screenTitle }>
					<Col>
						<Text style={{ fontWeight: 'bold', fontSize: 19, color: "#FFFFFF"  }}>
							Mis notificaciones
						</Text>
					</Col>
					<Col style={{ width: '12%', paddingBottom: 10 ,alignSelf: 'center', alignItems: 'center', alignContent: 'center'}}>
						<TouchableOpacity 
							onPress={()=>{ this.setState({ isCollapse: !this.state.isCollapse }) }} 
							style={{ alignSelf: 'center'  }}
						>
							<Icon name={"md-options"} size={32}  color={"#ffffff"} />
						</TouchableOpacity>
					</Col>
				</Row>
				<Collapsible collapsed={this.state.isCollapse} style={styles.collapseMenu}>
					<Row>
						<Col>
							<CheckBox 
								title="Ver solo favoritos" 
								containerStyle ={styles.containerCheckboxStyle}
								textStyle={styles.textCheckboxStyle}
								checked={ !this.state.viewAll }
								checkedColor={"#FFFFFF"}
								uncheckedColor ={"#ffffff"}
								onPress={()=>{ this._onCheckboxesPresed(false) }}
							/>
						</Col>
						<Col>
							<CheckBox 
								title="Ver todos" 
								containerStyle ={styles.containerCheckboxStyle}
								textStyle={styles.textCheckboxStyle}
								checked={this.state.viewAll}
								checkedColor={"#FFFFFF"}
								uncheckedColor={"#ffffff"}
								onPress={()=>{ this._onCheckboxesPresed(true) }}
							/>
						</Col>
					</Row>
				</Collapsible>
				{this._renderNotifications()}
			</Grid>

		)
	}
}


function mapStateToProps(state){
	var { notifications, isFetch } = state.Notifications
	return {
		notifications,
		isFetch
	}
}

function mapDispatchToProps(dispatch){
	return {
		dispatch: ()=>{
			dispatch( notifications(GET_NOTIFICATIONS, []) );
		}
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(NotificationsScreen);