export const topStyles = {
	backgroundColor: '#ef5350',
	marginBottom: 0
}

export const botStyles = {
	backgroundColor: '#eeeeee'
}

export const statusBarStyles = {
	backgroundColor: '#e53935',
	translucent: false
}

export const cardStyles = {
	width: '85%',
	marginTop: -10000,
	borderRadius: 12,
	bottom: 0,
	right: 0,
	left: 0,
}

export const FormStyles = {
	underlineColorAndroid: topStyles.backgroundColor,
	inputStyles: {
		textAlign: 'justify'
	}
}

export const ButtonStyle = {
	backgroundColor: topStyles.backgroundColor
}

export const linksStyles = {

	touchableContent: {
		marginBottom: 22,
		marginTop: 10,
	},
	textStyle: {
		color: topStyles.backgroundColor,
		fontSize: 14,
		fontWeight: 'bold',
		fontFamily: 'Roboto',
		right: "-77%",
		bottom: 0,
		top: 0 
	}
}


export const logoStyle = {
	width: 120
}