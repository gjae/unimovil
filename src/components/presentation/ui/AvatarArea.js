import React from 'react';
import { Avatar } from 'react-native-elements';
import { Text, View } from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';


const AvatarArea = (props) =>(

	<Col style={{ alignItems:'center', alignContent: 'center', alignSelf: 'center'  }}>
		<Avatar
			source={props.avatar}
			large
			rounded
			style={{
				borderWidth: 2,
				borderColor: '#ffb300'
			}}
		/>
		<Text
			style={{
				color: '#FFFFFF',
				fontSize: 18,
				fontWeight: 'bold',
				fontFamily: 'Roboto' 
			}}
		>
			{props.show_name}
		</Text>
	</Col>	
)

export default AvatarArea