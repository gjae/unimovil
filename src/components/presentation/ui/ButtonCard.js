import React from 'react';
import { connect } from 'react-redux';
import { TouchableOpacity, Text } from 'react-native';
import { Col, Row } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Ionicons';
import PropTypes from 'prop-types';

const ButtonCard = (props) =>(

	<Col >
		<Row size={ 22 }>
			<Col>
				<TouchableOpacity style={props.styles}>
					<Icon name={props.icon_name} size={44} color={props.icon_color} />
					<Text>{ props.button_text }</Text>
				</TouchableOpacity>
			</Col>
		</Row>
	</Col>
)

ButtonCard.propTypes = {
	icon_name: PropTypes.string.isRequired,
	button_text: PropTypes.string.isRequired,
	icon_color: PropTypes.string.isRequired
}


export default ButtonCard