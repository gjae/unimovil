import React from 'react';
import {
	FormInput,
	FormLabel,
	Button
} from 'react-native-elements';

import {
	View,
	TextInput,
	TouchableHighlight,
	Text,
	KeyboardAvoidingView,
	AsyncStorage
} from 'react-native';
 
import {
	LoginStyles
} from '../styles';

import {
	Grid,
	Row,
	Col
} from 'react-native-easy-grid';

import Icon from 'react-native-vector-icons/Ionicons';


const LoginForm =({ onChangeLoginText, onChangePassword, onPress})=> (
	<KeyboardAvoidingView>
		<FormLabel>Ingrese su correo </FormLabel>
		<FormInput 
			underlineColorAndroid={ LoginStyles.FormStyles.underlineColorAndroid } 
			inputStyle={ LoginStyles.FormStyles.inputStyles }
			onChangeText={(text)=> { onChangeLoginText(text) }}
		/>


		<FormLabel>Contraseña</FormLabel>
		<FormInput 
			secureTextEntry
			underlineColorAndroid={ LoginStyles.FormStyles.underlineColorAndroid } 
			inputStyle={ LoginStyles.FormStyles.inputStyles }
			onChangeText={ (text)=>{ onChangePassword(text) } }
		/>

		<TouchableHighlight style={ LoginStyles.linksStyles.touchableContent }>
			<Text style={ LoginStyles.linksStyles.textStyle }>
				Afiliarme
			</Text>
		</TouchableHighlight>

		<Button
			title={"Ingresar"}
			buttonStyle={LoginStyles.ButtonStyle}
			onPress={ ()=> {onPress()} }
		/>
	</KeyboardAvoidingView>
)

export default LoginForm;