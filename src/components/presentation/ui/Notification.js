import React from 'react';
import {
	View,
	Text,
	StyleSheet,
	Dimensions,
	TouchableOpacity
} from 'react-native';
import { Divider } from 'react-native-elements';

import { Col , Row } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Ionicons';


const Notification =(props) =>(
	<Col style={styles.superiorCol}>
		<Row>
			<Text style={styles.title}>{props.title}</Text>
			<Divider />
		</Row>
		<Row>
			<Text>{props.short_text}</Text>
		</Row>
		
		<Divider />
		<Row style={{ paddingLeft: 6 }}>
			<Col style={{ alignSelf: 'center', alignItems: 'center', alignContent: 'center' }}>
				<TouchableOpacity>
					<Icon name={"md-chatboxes"} size={ 35 } style={{ marginLeft: 14 }} />
				</TouchableOpacity>
			</Col>
			<Col style={{ alignSelf: 'center', alignItems: 'center', alignContent: 'center' }}>
				<TouchableOpacity>
					<Icon name={ 'ios-heart' } color={ props.favorite ? props.favoriteColor: props.unFavoriteColor }  size={ 35 } style={{ marginLeft: 14 }} />
				</TouchableOpacity>
			</Col>
		</Row>
	</Col>
)


var { height, width } = Dimensions.get('window');

const styles = StyleSheet.create({
	title: {
		fontSize: 17,
		fontWeight: 'bold' ,
		color: '#000000'
	},
	superiorCol: {
		width: '95%',
		paddingLeft: 7 ,
		height: ( height * 19 ) / 100
	},
	inferiorDivider: {
		top: 0,
		right: 0,
		left: 0
	}
})

export default Notification;