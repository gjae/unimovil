import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation';
import { Alert } from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware  from 'redux-saga';
import ButtonApp from '../redux/reducers/';
import LoginScreen from '../components/presentation/screens/LoginScreen';
import {mainTabNavigation} from './navigations';

import rootSaga  from '../redux/sagas';

const sagaMiddleware = createSagaMiddleware();

let store = createStore( ButtonApp, applyMiddleware(sagaMiddleware) );
sagaMiddleware.run(rootSaga);


// NAVIGACION INFERIOR DE LA APP


const App = createStackNavigator({
	Login: {
		screen: LoginScreen,
		navigationOptions: {
			header: () => null
		}
	},
	Session: {
		screen: mainTabNavigation,
		navigationOptions: {
			header: ()=> null
		}
	}
});


const Navigation = () => (
	<Provider store={ store } >
		<App />
	</Provider>
)

export default Navigation;