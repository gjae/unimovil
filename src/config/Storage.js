import PouchDB from 'pouchdb-core';

PouchDB.plugin( require('pouchdb-adapter-asyncstorage').default );

const Users = new PouchDB('@users:storage', { adapter: 'asyncstorage'});

export {
	Users
}