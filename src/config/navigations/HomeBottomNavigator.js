import React, { Component } from 'react';
import { createBottomTabNavigator } from 'react-navigation';

import HomeScreen from '../../components/presentation/screens/HomeScreen';
import Notifications from '../../components/presentation/screens/NotificationsScreen';
import Settings from '../../components/presentation/screens/SettingsScreen';
import Icon from 'react-native-vector-icons/Ionicons';
import {
	LoginStyles
} from '../../components/presentation/styles';

const mainTabNavigation = createBottomTabNavigator({
	Home: {
		screen: HomeScreen,
		navigationOptions:({navigation})=> ({
			header: () => null,
			tabBarIcon: (state)=> ( <Icon name="ios-home" size={37} color={ state.tintColor  } /> )
		})
	},
	Notifications:  {
		screen: Notifications,
		navigationOptions: ({navigation})=> ({
			header: () => null,
			tabBarIcon: (state)=> ( <Icon name="ios-notifications" size={37} color={ state.tintColor } />)
		})
	},
	Settings: {
		screen: Settings,
		navigationOptions:({navigation})=> ( {
			header: () => null,
			swipeEnable: true,
			tabBarIcon: (state)=> ( <Icon name="ios-settings" size={37} color={ state.tintColor } />)
		})
	}
},{
	tabBarOptions: {
		style: {
			backgroundColor: '#ffffff',
			borderWidth: 0,
			elevation: 0,
			borderTopWidth: 0
		},
		showLabel: false,
		activeTintColor: LoginStyles.topStyles.backgroundColor,
		inactiveTintColor: "#000000",

	},
	backBehavior: 'initialRoute'
});

export default mainTabNavigation;