// ACTIONS PARA EL LOGIN
export const SEND_CREDENTIALS = 'SEND_CREDENTIALS';
export const SET_SESSION = 'SAVE_SECTION_ON_LOCAL_STORAGE';

export const sendCredentials = ( credentials, navigation ) => {
	return {
		type: SEND_CREDENTIALS,
		credentials,
		navigation
	}
};


// ACTIONS PARA LA VISTA DE NOTIFICACIONES

export const GET_NOTIFICATIONS = 'GET_NOTIFICATIONS';
export const SET_NOTIFICATIONS = 'SET_NOTIFICATIONS';
export const SET_NOTIFICATIONS_FILTER = 'VIEW_ALL_NOTIFICATIONS';

export const notifications = ( type, notificationsArray = [], isFetch = false ) =>{
	return {
		type,
		notificationsArray,
		isFetch
	}
}

export const viewNotifications = ( type, notificationsArray = [], viewAll = true ) =>{
	return {
		type, 
		notificationsArray,
		viewAll
	}
}