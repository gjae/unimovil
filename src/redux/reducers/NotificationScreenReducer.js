import {
	GET_NOTIFICATIONS,
	SET_NOTIFICATIONS,
	SET_NOTIFICATIONS_FILTER,
	notifications
} from '../actions';
import { Alert } from 'react-native';



export const Notifications = ( state = { notifications: []}, action = { notificationsArray: [] } ) =>{
	switch( action.type ){
		case SET_NOTIFICATIONS:{
			return Object.assign({}, state, {
				notifications: action.notificationsArray
			});
		}

		case SET_NOTIFICATIONS_FILTER: {
			Alert.alert("NOTIFICATIONS", JSON.stringify(action));
			return state;
		}

		default: {
			return state;
		}
	}

}