import { LoginReducer } from './LoginReducer';
import { Notifications } from './NotificationScreenReducer';
import { combineReducers } from 'redux';

const rootReducer = combineReducers({
	LoginReducer,
	Notifications
});

export default rootReducer;