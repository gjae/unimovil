import { takeEvery, put, all } from 'redux-saga/effects';
import { 
	SEND_CREDENTIALS,
	SET_SESSION,
	GET_NOTIFICATIONS,
	SET_NOTIFICATIONS,
	notifications
} from '../actions';

import rootReducer from '../reducers';

import {Alert} from 'react-native';

export function* loginSaga(){

	/**
	*	ESPERA A QUE SE DISPARE UNA ACCION DE TIPO SEND_CREDENTIALS,
	*	PARA PEDIR VIA AJAX A LA API LA SESSION Y EL TOKEN
	*/
	yield takeEvery( SEND_CREDENTIALS, (action)=>{
		action.navigation.navigate('Session');
	})
}

export function* setNotifications(action){
	
	yield put( notifications(SET_NOTIFICATIONS, [], true) );

	var notificationsArray = [
		{
			id: 1,
			title: "Lorem ipsum",
			short_text: `Texto de prueba`,
			favorite: false
		},
		{
			id: 2,
			title: "Lorem ipsum",
			short_text: `Texto de prueba 2`,
			favorite: true
		},
		{
			id: 3,
			title: "Lorem ipsum",
			short_text: `Texto de prueba 3`,
			favorite: true
		},
		{
			id: 4,
			title: "Lorem ipsum",
			short_text: `Texto de prueba 3`,
			favorite: false
		},
		{
			id: 5,
			title: "Lorem ipsum",
			short_text: `Texto de prueba 3`,
			favorite: false
		},
		{
			id: 6,
			title: "Lorem ipsum",
			short_text: `Texto de prueba 3`,
			favorite: false
		},
		{
			id: 7,
			title: "Lorem ipsum",
			short_text: `Texto de prueba 3`,
			favorite: false
		},
		{
			id: 8,
			title: "Lorem ipsum",
			short_text: `Texto de prueba 3`,
			favorite: false
		},
		{
			id: 9,
			title: "Lorem ipsum",
			short_text: `Texto de prueba 3`,
			favorite: false
		},
		{
			id: 10,
			title: "Lorem ipsum",
			short_text: `Texto de prueba 3`,
			favorite: false
		},
		{
			id: 11,
			title: "Lorem ipsum",
			short_text: `Texto de prueba 3`,
			favorite: false
		},
		{
			id: 12,
			title: "Lorem ipsum",
			short_text: `Texto de prueba 3`,
			favorite: false
		},
	];
	yield put( notifications(SET_NOTIFICATIONS, notificationsArray, false) );
	
}

export function* notificationsSaga(){
	yield takeEvery(GET_NOTIFICATIONS, setNotifications);
}

function* rootSaga(){
	yield all([
		loginSaga(),
		notificationsSaga()
	]);
}

export default rootSaga;